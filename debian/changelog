python-project-generator (0.12.0-2) unstable; urgency=medium

  * d/patches:
    - Add patch fix-invalid-escape-sequences-in-re-match
    - Add patch use-comments-for-comments
    - Add patch rename-nose-specific-test-setup-function
  * d/s/options:
    - Ignore template files with CRLF/LF issues
  * d/s/options: Tweak regex for ignored template files

 -- Nick Morrott <nickm@debian.org>  Sun, 11 Feb 2024 04:14:14 +0000

python-project-generator (0.12.0-1) unstable; urgency=medium

  * New upstream version 0.12.0
  * d/control:
    - Declare compliance with Debian Policy 4.6.2 (no changes)
    - Add pybuild-plugin-pyproject to B-D
  * d/copyright:
    - Bump years of Debian copyright
    - Add copyright details for new llvm_arm.py tool
  * d/clean:
    - Remove pandoc generated manpage files and egginfo
  * d/lintian-overrides:
    - Refresh override strings to match new format
    - Add override for Python metadata
  * d/s/lintian-overrides:
    - Refresh override strings to match new format

 -- Nick Morrott <nickm@debian.org>  Mon, 05 Feb 2024 02:45:55 +0000

python-project-generator (0.11.3-1) unstable; urgency=medium

  * New upstream version 0.11.3
  * d/control:
    - Add python3-setuptools-scm to B-D
    - Drop python3-nose from B-D
  * d/copyright:
    - Refresh upstream Files copyright
  * d/patches:
    - Refresh drop-alternative-binary-entrypoint (upstream changes)
  * d/s/lintian-overrides:
    - Add overrides for long lines (2 files)
  * d/t/python3-project-generator:
    - Tweak test script to work with new ._version.py file

 -- Nick Morrott <nickm@debian.org>  Thu, 24 Mar 2022 03:59:07 +0000

python-project-generator (0.11.1-1) unstable; urgency=medium

  * New upstream version 0.11.1
  * d/control:
    - Update standards version to 4.6.0, no changes needed.
  * d/copyright:
    - Refresh years of Debian copyright
  * d/salsa-ci.yml:
    - Fix missing newline from salsa web editor
  * d/t/control:
    - Add python3-all to test dependencies
  * d/watch:
    - Update pattern for GitHub archive URLs
  * d/rules: Clean out new build artefact

 -- Nick Morrott <nickm@debian.org>  Sun, 06 Feb 2022 03:31:45 +0000

python-project-generator (0.11.0-1) unstable; urgency=medium

  * New upstream version 0.11.0
  * d/copyright:
    - Add copyright information for upstream additions
  * d/progen.1.md.in:
    - Refresh manpage with new progen command options
  * d/salsa-ci.yml:
    - Add Salsa CI pipeline

 -- Nick Morrott <nickm@debian.org>  Sun, 07 Feb 2021 06:31:18 +0000

python-project-generator (0.10.0-3) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control:
    - Update Maintainer field with new Debian Python Team contact address.
    - Update Vcs-* fields with new Debian Python Team Salsa layout.

  [ Debian Janitor ]
  * d/u/metadata:
    - Remove obsolete field Contact (already present in machine-readable
      debian/copyright).

  [ Nick Morrott ]
  * d/control:
    - Declare compliance with Debian Policy 4.5.1
  * d/copyright:
    - Refresh years of Debian copyright
  * d/s/lintian-overrides:
    - Add override for omitted SCM/CI files

 -- Nick Morrott <nickm@debian.org>  Fri, 08 Jan 2021 03:13:50 +0000

python-project-generator (0.10.0-2) unstable; urgency=medium

  * d/manpages:
    - Improve formatting of custom manpage

 -- Nick Morrott <nickm@debian.org>  Fri, 31 Jul 2020 19:44:36 +0100

python-project-generator (0.10.0-1) unstable; urgency=medium

  * New upstream version 0.10.0
  * d/manpages:
    - Update custom progen manpage

 -- Nick Morrott <nickm@debian.org>  Fri, 31 Jul 2020 12:25:31 +0100

python-project-generator (0.9.17-2) unstable; urgency=medium

  * d/control:
    - Reformat (build) dependencies
    - Bump Standards-Version to 4.5.0
    - Bump debhelper-compat level to 13
  * d/copyright:
    - Refresh years of Debian copyright
  * d/tests:
    - Run autopkgtests against supported Python versions

 -- Nick Morrott <nickm@debian.org>  Fri, 19 Jun 2020 02:50:13 +0100

python-project-generator (0.9.17-1) unstable; urgency=medium

  * New upstream version 0.9.17
  * d/copyright:
    - Refresh Debian Files stanza
  * progen documentation:
    - Refresh for added subcommand arguments
    - Refresh author details

 -- Nick Morrott <nickm@debian.org>  Mon, 25 Nov 2019 20:05:47 +0000

python-project-generator (0.9.16-1) unstable; urgency=medium

  * New upstream version 0.9.16
  * d/control:
    - Reformat (cme fix)
  * d/patches:
    - Refresh patch drop-alternative-binary-entrypoint (changes)

 -- Nick Morrott <nickm@debian.org>  Sun, 17 Nov 2019 16:12:50 +0000

python-project-generator (0.9.15-1) unstable; urgency=medium

  * New upstream version 0.9.15
  * d/patches:
    - Drop fix-iteration-over-dict-python3.8.patch (applied upstream)

 -- Nick Morrott <nickm@debian.org>  Fri, 08 Nov 2019 12:15:41 +0000

python-project-generator (0.9.13-2) unstable; urgency=medium

  [Emmanuel Arias]
  * Fix iterating over a dict while removing elements for
    Python 3.8. Thanks to Matthias Klose for the patch.
    (Closes: #944159)
  * Bump dehelper-compat to 12.

  [ Ondřej Nový ]
  * Use debhelper-compat instead of debian/compat.
  * Bump Standards-Version to 4.4.1.

  [ Nick Morrott ]
  * d/control:
    - Add Rules-Requires-Root field
    - Uploaders: use my new Debian email address
  * d/copyright:
    - Update years of Debian copyright
  * d/gbp.conf:
    - Use pristine-tar
  * d/patches/fix-iteration-over-dict-python3.8.patch:
    - Add patch header

 -- Nick Morrott <nickm@debian.org>  Wed, 06 Nov 2019 02:54:02 +0000

python-project-generator (0.9.13-1) unstable; urgency=medium

  * Initial release (Closes: #913923)

 -- Nick Morrott <knowledgejunkie@gmail.com>  Sun, 30 Dec 2018 12:28:51 +0100
