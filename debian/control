Source: python-project-generator
Maintainer: Debian QA Group <packages@qa.debian.org>
Section: python
Priority: optional
Build-Depends:
 debhelper-compat (= 13),
 dh-python,
 pandoc,
 pybuild-plugin-pyproject,
 python3-all,
 python3-jinja2,
 python3-project-generator-definitions (<< 0.3.0),
 python3-pytest,
 python3-setuptools,
 python3-setuptools-scm,
 python3-xmltodict,
 python3-yaml
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/python-team/packages/python-project-generator
Vcs-Git: https://salsa.debian.org/python-team/packages/python-project-generator.git
Homepage: https://github.com/project-generator/project_generator
Rules-Requires-Root: no

Package: python3-project-generator
Architecture: all
Depends:
 python3-project-generator-definitions (<< 0.3.0),
 ${misc:Depends},
 ${python3:Depends}
Description: project generators for various embedded tools (IDE)
 python-project-generator allows one to define an embedded hardware/software
 project in structured text (using YAML), and then generate IDE (integrated
 development environment) project files based on the defined rules.
 .
 python-project-generator currently generates projects for the following tools:
 .
  - CMake (GCC ARM)
  - CoIDE (GCC ARM)
  - Eclipse (Makefile with GCC ARM)
  - IAR
  - Makefile (ARMCC)
  - Makefile (GCC ARM)
  - Sublime Text (Makefile with GCC ARM)
  - uVision4 and uVision5
  - Visual Studio (Makefile with GCC ARM)
 .
 This package installs the library and `progen` helper script for Python 3.
